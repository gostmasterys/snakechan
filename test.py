#!/bin/python2.7
# -*- coding: utf-8 -*-

import mock
import unittest
import logging
import sys
import socket
import struct

from SnakeChan import SnakeChan

# cli.connect_socket = MagicMock()

# cli.connect(('127.0.0.1', 1234))
# print(cli)
# mock.assert_called_once_with('connect')

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

log = logging.getLogger('Unittest')
cli = SnakeChan()


class SnakeChanTestCase(unittest.TestCase):

    @mock.patch('__main__.cli.connect_socket')
    def test_connect(self, mock_socket):
        """ Test the connection sequence """

        print("\n")
        connect_address = ('127.0.0.1', 1234)

        cli.connect(connect_address, test=1, test2='42')
        token = cli.connects_info[connect_address]["token"]
        outbound_seq = '\xff\xff\xff\xff'

        mock_socket.sendto.assert_called_once_with('{}GetToken {} Snake'.format(outbound_seq, token), connect_address)
        mock_socket.reset_mock()

        log.info("GetToken OK -> Check response when socket timout")

        mock_socket.recvfrom.side_effect = socket.timeout
        cli.iterate()
        mock_socket.sendto.assert_called_once_with('{}GetToken {} Snake'.format(outbound_seq, token), connect_address)
        mock_socket.reset_mock()

        log.info("GetToken OK -> Check response when socket is non-blocking")

        mock_socket.recvfrom.side_effect = socket.error
        time = 0
        while time < 3000:
            time += 100
            cli.iterate(100)

        mock_socket.sendto.assert_called_once_with('{}GetToken {} Snake'.format(outbound_seq, token), connect_address)
        mock_socket.reset_mock()

        log.info("GetToken OK -> Send Token response")

        mock_socket.recvfrom.side_effect = None
        mock_socket.recvfrom.return_value = ('{}Token 494447609 {} 19'.format(outbound_seq, token), connect_address)
        cli.iterate()
        mock_socket.sendto.assert_called_once_with('{}Connect \\challenge\\494447609\\protocol\\19\\test\\1\\test2\\42'.format(outbound_seq), connect_address)
        mock_socket.reset_mock()

        @cli.onAccept
        def check_onAccept(src):
            assert src == connect_address
            log.info("onAccept callback OK")

        log.info("Connect response OK -> Send Connect acceptation")

        mock_socket.recvfrom.return_value = ('{}Connected 494447609'.format(outbound_seq), connect_address)
        cli.iterate()

        @static_vars(msg_counter=0)
        @cli.onReceive
        def check_onReceive(data, src):
            if check_onReceive.msg_counter == 0:
                assert src == connect_address
                assert data == "msg test 3"
                log.info("Received msg OK -> Send msg with a lower sequence")

            elif check_onReceive.msg_counter == 1:
                assert src == connect_address
                assert data == "msg test 4"
                log.info("Lower sequence ingored OK")

            check_onReceive.msg_counter += 1

        print("Test messages reception")
        log.info("Send msg")
        mock_socket.recvfrom.return_value = ('{}msg test 3'.format(struct.pack('!I', 3)), connect_address)
        cli.iterate()
        mock_socket.recvfrom.return_value = ('{}msg test 2'.format(struct.pack('!I', 2)), connect_address)
        cli.iterate()
        mock_socket.recvfrom.return_value = ('{}msg test 4'.format(struct.pack('!I', 4)), connect_address)
        cli.iterate()

        print("Test messages transmition")
        cli.send("send test 1", connect_address)
        mock_socket.sendto.assert_called_once_with('{}send test 1'.format(struct.pack('!I', 1)), connect_address)
        mock_socket.reset_mock()

        cli.send("send test 2", connect_address)
        mock_socket.sendto.assert_called_once_with('{}send test 2'.format(struct.pack('!I', 2)), connect_address)
        mock_socket.reset_mock()

# @todo server on connect

if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr)
    logging.getLogger('SnakeChan').setLevel(logging.CRITICAL)
    logging.getLogger('Unittest').setLevel(logging.DEBUG)

    suite = unittest.TestLoader().loadTestsFromTestCase(SnakeChanTestCase)

    print("--- Starting Tests ---")
    unittest.TextTestRunner(verbosity=2).run(suite)
