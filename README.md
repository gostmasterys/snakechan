# README - SnakeChan #

### What is this repository for? ###

* Quick summary - This is a connection protocol for a snake game using UDP
* Version - 2.0.0

### How do I get set up? ###

Install dependecies with
> pip install -r requirements.txt

Put the SnakeChan.py file in your project and import the needed classes.


### Demonstration example ###

A basic communication (periodic send of message client ->serveur, then answer from the server) can be obtained using the following commands.

Server's side:

> python2.7 ./SnakeChan.py <port>

Client's side:

> python2.7 ./SnakeChan.py <ip> <port>

### Who do I talk to? ###

* jonataubert [at] gmail [dot] com
* meister.thierry [at] gmail [dot] com