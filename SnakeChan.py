#!/bin/python2.7
# -*- coding: utf-8 -*-


# @todo wait on connect

import socket
import struct
import time
import sys
import logging
import random as r


log = logging.getLogger('SnakeChan')


class ConnectState(object):
    """ State of the client's connection of SnakeChan protocol
    (Used on the client's side)"""

    START = 0
    WAIT_TOKEN = 1
    WAIT_CONNECTION = 2


class SnakeChan(object):
    def __init__(self, socket_timeout=3, connection_timeout=3):
        self.PROTOCOL = 19
        self.OUTBOUND_SEQ = 0xffffffff

        self.CONNECTION_TIMEOUT = connection_timeout
        self.SOCKET_TIMEOUT = socket_timeout
        self.MAX_POSITIV_DIFF = 1000    # next nb of sequences accepted before invalide
        self.MAX_DATAGRAM_SIZE = 1024

        self.wait_time = 0

        self.connect_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.connect_socket.settimeout(self.SOCKET_TIMEOUT)

        # self.try_connecting[addr] = ConnectState.START

        self.port = 0

        # Nodes that this node is connected to
        self.connects_info = {}
        self.connects = []
        self.try_connecting = {}

        # Nodes connected to this node
        self.incomming_connects_info = {}
        self.incomming_connects = []

        # User callback
        self.receptionCallback = None
        self.acceptCallback = None
        self.connectionCallback = None

        # logging.basicConfig(stream=sys.stderr)

    @staticmethod
    def __packetize(data, header):
        """ Creates datagram for the SnakeChan protocol"""

        return struct.pack('!I', header) + data

    @staticmethod
    def __unPacketize(raw_data):
        """ Extract datagram of the SnakeChan protocol"""

        return struct.unpack('!I', raw_data[:4])[0], raw_data[4:]

    def __isNewSeq(self, new_seq, old_seq):
        """Check if new_seq is a bigger number than old_seq. Take the overflow into account."""

        if ((new_seq - old_seq) & 0xffffffff) < self.MAX_POSITIV_DIFF:
            return True
        return False

    def connect(self, dest_address, token=r.randint(0, 2**32-1), protocol=None, **kwargs):
        #Send the token demand
        if dest_address not in self.connects_info:
            self.connects_info[dest_address] = {}

        if "token" not in self.connects_info[dest_address]:
            self.connects_info[dest_address]["token"] = token
        self.connects_info[dest_address]["seq"] = 0
        if protocol is None:
            kwargs['protocol'] = self.PROTOCOL
        else:
            kwargs['protocol'] = protocol
        self.connects_info[dest_address]["connect_data"] = kwargs
        self.send("GetToken {} Snake".format(self.connects_info[dest_address]["token"]), dest_address)
        #new state
        self.try_connecting[dest_address] = ConnectState.WAIT_TOKEN
        log.debug(" State : WAIT_TOKEN")

    def listen(self, port=1234):
        """ listen on any ip for a given port """

        self.port = port
        self.connect_socket.bind(("0.0.0.0", self.port))

    def onReceive(self, callback):
        """ Define the function to be called when a message is received """

        self.receptionCallback = callback
        return callback

    def onConnect(self, callback):
        """ Define the function to be called when an incomming connection occures """

        self.connectionCallback = callback
        return callback

    def onAccept(self, callback):
        """ Define the function to be called when a connection is established """

        self.acceptCallback = callback
        return callback

    def send(self, data, dest=None):
        """Send data through the SnakeChan protocol"""

        if len(data) > self.MAX_DATAGRAM_SIZE:
            raise ValueError("Too much data (>{})".format(self.MAX_DATAGRAM_SIZE))

        if dest is None:
            if self.connects != []:
                dest = self.connects[0]
            elif self.try_connecting != {}:
                dest = list(self.try_connecting.viewkeys())[0]
            else:
                raise AttributeError("Unknown message destination")

        if dest in self.connects:
            self.connects_info[dest]["send_seq"] = (self.connects_info[dest]["send_seq"] + 1) & 0xffffffff
            seq = self.connects_info[dest]["send_seq"]

        elif dest in self.incomming_connects:
            self.incomming_connects_info[dest]["send_seq"] = (self.incomming_connects_info[dest]["send_seq"] + 1) & 0xffffffff
            seq = self.incomming_connects_info[dest]["send_seq"]
        else:
            seq = self.OUTBOUND_SEQ

        log.debug(" -> {} : {} : {}".format(seq, dest, data))
        snake_p_datagram = SnakeChan.__packetize(data, seq)
        self.connect_socket.sendto(snake_p_datagram, dest)

    def iterate(self, iterate_time=20):

        snake_p_datagram = ""
        addr = ()

        try:
            snake_p_datagram, addr = self.connect_socket.recvfrom(self.MAX_DATAGRAM_SIZE)
        except socket.timeout:
            #Reinit connection process when no valid msg received
            for addr in self.try_connecting:
                self.connect(addr, **self.connects_info[addr]["connect_data"])
            return
        except socket.error:
            self.wait_time += iterate_time
            if self.wait_time >= 3000:
                self.wait_time = 0
                for addr in self.try_connecting:
                    self.connect(addr, **self.connects_info[addr]["connect_data"])
            return

        incomming_seq, r_msg = SnakeChan.__unPacketize(snake_p_datagram)

        log.debug(" <- {} : {} : {}".format(incomming_seq, addr, r_msg))

        # Messages from nodes that this node is connected to
        if addr in self.connects:
            #when valid seqNumber, update it and get data without seq number
            if self.__isNewSeq(incomming_seq, self.connects_info[addr]["seq"]):
                self.connects_info[addr]["seq"] = incomming_seq
                #give the addr and data to upper layer
                self.receptionCallback(r_msg, addr)

        # Messages from nodes connected to this one
        elif addr in self.incomming_connects:
            #when valid seqNumber, update it and get data without seq number
            if self.__isNewSeq(incomming_seq, self.incomming_connects_info[addr]["serv_seq"]):
                self.incomming_connects_info[addr]["serv_seq"] = incomming_seq
                #give the addr and data to upper layer
                self.receptionCallback(r_msg, addr)

        elif incomming_seq == self.OUTBOUND_SEQ:

            # Message receive from a server (node we try to connect to)
            if addr in self.try_connecting:

                if self.try_connecting[addr] == ConnectState.WAIT_TOKEN:

                    #Check if r_msg is valid
                    if r_msg is not None and "Token" in r_msg:
                        # Data parsing
                        data = r_msg.split(" ")
                        try:
                            # More verification on the validity of the message
                            if len(data) == 4 and int(data[2]) == self.connects_info[addr]["token"]:
                                self.connects_info[addr]["serv_token"] = int(data[1])
                                self.connects_info[addr]["serv_protocol"] = int(data[3])

                                # Prepare the connecting message
                                protocol = self.connects_info[addr]["connect_data"].pop('protocol')
                                formated_connect_data = ""
                                for param, val in self.connects_info[addr]["connect_data"].items():
                                    formated_connect_data += "\\{}\\{}".format(param, val)

                                self.send("Connect \\challenge\\{}\\protocol\\{}".format(self.connects_info[addr]["serv_token"], protocol)+formated_connect_data, addr)

                                self.try_connecting[addr] = ConnectState.WAIT_CONNECTION
                                log.debug(" State : WAIT_CONNECTION")

                        except ValueError, e:
                            log.debug(" Error : "+str(e))
                            self.try_connecting[addr] = ConnectState.START

                elif self.try_connecting[addr] == ConnectState.WAIT_CONNECTION:
                    #if connection accepted
                    if r_msg is not None and "Connected" in r_msg and "{}".format(self.connects_info[addr]["serv_token"]) in r_msg:
                        if self.acceptCallback is not None:
                            self.acceptCallback(addr)
                        self.connects_info[addr]["send_seq"] = 0
                        self.connects.append(addr)
                        del(self.try_connecting[addr])

                        log.debug(" State : CONNECTED")

            # Message receive from a potential client (also OUTBOUND_SEQ)
            else:
                data = r_msg.split(' ')

                # New client -> add it to dict
                if addr not in self.incomming_connects_info:
                    self.incomming_connects_info[addr] = dict()
                    self.incomming_connects_info[addr]["serv_token"] = r.randint(0, 2**32-1)

                # When Get token -> add token to client
                if len(data) == 3 and data[0] == "GetToken" and data[2] == "Snake":
                    self.incomming_connects_info[addr]["cli_token"] = int(data[1])
                    self.send("Token {} {} {}".format(self.incomming_connects_info[addr]["serv_token"], self.incomming_connects_info[addr]["cli_token"], self.PROTOCOL), addr)

                # When Connect msg receive respond connected
                elif len(data) == 2 and data[0] == "Connect":
                    #when "connect" received and there is a key cli_seq for the client
                    if "cli_token" in self.incomming_connects_info[addr]:
                        values = data[1].split("\\")

                        #extract data in a dict
                        connect_msg = {}
                        for key, val in zip(values[1::2], values[2::2]):
                            connect_msg[key] = val

                        #Check if valid keys received and serv_sequence already generate
                        if "challenge" in connect_msg:
                            self.send("Connected {}".format(self.incomming_connects_info[addr]["serv_token"]), addr)
                            self.incomming_connects_info[addr]["last_msg_time"] = time.time()
                            #Store the first STARTing seq number as a token
                            self.incomming_connects_info[addr]["serv_seq"] = 0      # init seq numbers with 0
                            self.incomming_connects_info[addr]["send_seq"] = 0
                            self.incomming_connects.append(addr)
                            log.debug(" {} : Connected".format(addr))

                            if self.connectionCallback is not None:
                                self.connectionCallback(connect_msg, addr)

if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr)
    logging.getLogger('SnakeChan').setLevel(logging.DEBUG)

    #When server requested (the argument is the port number)
    if (len(sys.argv) == 2):
        print("Server STARTED on port : " + sys.argv[1])

        server = SnakeChan()

        @server.onReceive
        def respond(data, src):
            print("received {} from {}".format(data, src))
            server.send("Hello Client", src)

        server.listen(sys.argv[1])

        while True:
            time.sleep(0.5)
            server.iterate(500)

#When a client is requested (argument is ip and port to connect to)
    elif (len(sys.argv) == 3):
        print("Client STARTED, connect to ip " + sys.argv[1] + " ,port : " + sys.argv[2])
        client = SnakeChan()

        @client.onReceive
        def print_data(data, src):
            print(data)

        client.connect((sys.argv[1], int(sys.argv[2])))

        while True:
            time.sleep(0.5)
            client.send("Hello server!")
            print("send")
            client.iterate(500)
